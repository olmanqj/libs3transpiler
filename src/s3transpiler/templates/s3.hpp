/*
 * File automatically generated with s3transpiler
 *
 */ 

#include <s3/Param.hpp>
{% if groups_list -%}
#include <s3/Group.hpp>
{%- endif %}

using namespace s3;


{% if groups_list -%}
{% macro group_body(group) -%}
{{ 'extern Group<{}> {};'.format(group["params"]|length, group['name']) }}
{%- endmacro -%}

/*******************************************************************************
 * Groups
*******************************************************************************/
{% for group in groups_list -%}
    {{ group_body(group) }}
{% endfor -%}
{%- endif %}


{% macro sys_body(parent, sys_name) -%}
{%- if not parent %}
/*******************************************************************************
 * {{sys_name}}
*******************************************************************************/
{%- endif -%}
{%- endmacro -%}

{%- macro param_body(type, param) -%}
{%- if param["len"] == 1 -%}
    {{ 'extern Param<{}> {}_{};'.format(type, param["parent"], param["param"]) }}
{%- else -%}
    {{ 'extern ParamVec<{},{}> {}_{};'.format(type, param["len"], param["parent"], param["param"]) }}
{%- endif -%}
{%- endmacro -%}

{%- for sys in sys_list -%}
    {{ sys_body(sys["parent"], sys["sys"]) }}
{% for param in sys["params"] -%}
{% if param["type"] == "bool" -%}
    {{ param_body("bool", param) }}
{% elif param["type"] == "u8" -%}
    {{ param_body("uint8_t", param) }}
{% elif param["type"] == "i8" -%}
    {{ param_body("int8_t", param) }}
{% elif param["type"] == "u16" -%}
    {{ param_body("uint16_t", param) }}
{% elif param["type"] == "i16" -%}
     {{ param_body("int16_t", param) }}
{% elif param["type"] == "u32" -%}
    {{ param_body("uint32_t", param) }}
{% elif param["type"] == "i32" -%}
    {{ param_body("int32_t", param) }}
{% elif param["type"] == "u64" -%}
    {{ param_body("uint64_t", param) }}
{% elif param["type"] == "i64" -%}
    {{ param_body("int64_t", param) }}
{% elif param["type"] == "f32" -%}
    {{ param_body("float", param) }}
{% elif param["type"] == "f64" -%}
    {{ param_body("double", param) }}
{% elif param["type"] == "str" -%}
    {{ 'extern ParamStr<{}> {}_{};'.format(param["len"], param["parent"], param["param"]) }}
{% elif param["type"] == "blob" -%}
    {{ 'extern ParamBlob<{}> {}_{};'.format(param["len"], param["parent"], param["param"]) }}
{% elif param["type"] == "date" -%}
    {{ 'extern ParamDate {}_{};'.format(param["parent"], param["param"]) }}
{%- endif -%}
{%- endfor -%}
{%- endfor -%}


