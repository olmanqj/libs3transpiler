/*
 * File automatically generated with s3transpiler
 */ 

#include <s3/Param.hpp>
{% if groups_list -%}
#include <s3/Group.hpp>
{%- endif %}

using namespace s3;

{%- set signature_padding = 20-%}

{% macro sys_body(parent, sys_name, description) -%}
{%- if parent -%}
{{ '{: <20} {}({} "{}"); '.format("Sys", parent + '_' + sys_name, parent + ",", sys_name) }}
{%- if description -%}
{{ '  // {}'.format(description)}}
{%- endif -%}
{%- else %}
{% if description -%}
/*******************************************************************************
 * {{sys_name}}: {{description}}
*******************************************************************************/
{% else -%}
/*******************************************************************************
 * {{sys_name}}
*******************************************************************************/
{% endif -%}
{{ '{: <20} {}("{}"); '.format("Sys", sys_name, sys_name) }} 
{%- endif -%}
{%- endmacro -%}



{%- macro param_signature(type, param) -%}
{%- if   param["type"] == "str" -%}
{{ 'ParamStr<{}>'.format(param["len"]) }}
{%- elif param["type"] == "blob" -%}
{{ 'ParamBlob<{}>'.format(param["len"]) }}
{%- elif param["type"] == "date" -%}
{{ 'ParamDate'}}
{%- elif   param["len"] == 1 -%}
{{ 'Param<{}>'.format(type) }}
{%- else -%}
{{ 'ParamVec<{},{}>'.format(type, param["len"]) }}
{%- endif -%}
{%- endmacro -%}


{% macro param_body(type, param) -%}
{%- if   param["len"] == 1 -%}
{{ '{:<20} {}_{}({}, "{}", {});'.format(param_signature(type, param), param["parent"], param["param"], param["parent"], param["param"], param["default"]) }}
{%- else -%}
{{ '{:<20} {}_{}({}, "{}", {{{}}});'.format(param_signature(type, param), param["parent"], param["param"], param["parent"], param["param"],  param["default"]|join(', ')) }}
{%- endif -%}
{%- endmacro -%}


{% macro param_bool_body(type, param) -%}
{%- if   param["len"] == 1 -%}
{{ '{:<20} {}_{}({}, "{}", {});'.format(param_signature(type, param), param["parent"], param["param"], param["parent"], param["param"], param["default"]|replace("True", "true")|replace("False", "false")) }}
{%- else -%}
{{ '{:<20} {}_{}({}, "{}", {{{}}});'.format(param_signature(type, param), param["parent"], param["param"], param["parent"], param["param"],  param["default"]|join(', ')|replace("True", "true")|replace("False", "false")) }}
{%- endif -%}
{%- endmacro -%}



{%- for sys in sys_list -%}
    {{ sys_body(sys["parent"], sys["sys"], sys["desc"]) }}   
{%- for param in sys["params"] %}
{% if  param["type"] == "u8" -%}
    {{ param_body("uint8_t", param) }}
{%- elif param["type"] == "i8" -%}
    {{ param_body("int8_t", param) }}
{%- elif param["type"] == "u16" -%}
    {{ param_body("uint16_t", param) }}
{%- elif param["type"] == "i16" -%}
     {{ param_body("int16_t", param) }}
{%- elif param["type"] == "u32" -%}
    {{ param_body("uint32_t", param) }}
{%- elif param["type"] == "i32" -%}
    {{ param_body("int32_t", param) }}
{%- elif param["type"] == "u64" -%}
    {{ param_body("uint64_t", param) }}
{%- elif param["type"] == "i64" -%}
    {{ param_body("int64_t", param) }}
{%- elif param["type"] == "f32" -%}
    {{ param_body("float", param) }}
{%- elif param["type"] == "f64" -%}
    {{ param_body("double", param) }}
{%- elif  param["type"] == "bool" -%}
    {{ param_bool_body("bool", param) }}
{%- elif param["type"] == "date" -%}
    {{ param_body("ParamDate", param) }}
{%- elif param["type"] == "str" -%}
    {{ '{:<20} {}_{}({}, "{}", "{}");'.format(param_signature(type, param), param["parent"], param["param"], param["parent"], param["param"], param["default"]) }}
{%- elif param["type"] == "blob" -%}
    {{ '{:<20} {}_{}({}, "{}", "{}");'.format(param_signature(type, param), param["parent"], param["param"], param["parent"], param["param"], param["default"]) }}
{%- endif -%}
{%- if param["desc"] -%}
    {{ '  // {}'.format(param["desc"])}}
{%- endif -%}
{%- endfor %}

{% endfor -%}



{% if groups_list -%}
{% macro group_body(group) -%}
{{ 'Group<{}> {}("{}", {}'.format(group["params"]|length, group["name"], group["name"], "{") }}
{% for param in group["params"] -%}
    {{ "\t&{},".format(param["var_name"]) }}   
{% endfor -%}
{{- "});"}}
{% endmacro %}

/*******************************************************************************
 * Groups
*******************************************************************************/
{% for group in groups_list -%}
    {{ group_body(group) }}   
{% endfor -%}
{%- endif -%}
