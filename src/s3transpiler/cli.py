#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
s3 Space System Model Transpiler

Tool to generate code from a s3 Model.

usage: 

s3transpiler.py [-h] [-o OUT] [-g] [-f FILTER [FILTER ...]] [-v] file

Tool to generate code from s3 Model.

positional arguments:
  file                  Path of input s3 model [.cpp, .hpp, .csv].

optional arguments:
  -h, --help            show this help message and exit
  -o OUT, --out OUT     Name of the output file. Target format will be deduced
                        from provided file extension [.cpp, .hpp, .json,
                        .yaml, .md].
  -g, --groups          Include groups source code. Only valid for source code
                        outputs [.cpp, .hpp].
  -f FILTER [FILTER ...], --filter FILTER [FILTER ...]
                        Filter Params by groups.
  -v, --verbose         Display extra info.
'''

import argparse
from .s3transpiler import S3Transpiler


################################################################################
# Run as CLI
################################################################################

def s3tp_cli():
    arg_parser = argparse.ArgumentParser(description='Tool to generate code a and documentation from s3 Model.')
    
    # General arguments
    arg_parser.add_argument('file', type=str,
                             help='Path of input s3 model [.yaml, .json, .csv].')

    arg_parser.add_argument('-o', '--out', type=str, default=None,
                            help='Name of the output file. Target format will be deduced from provided file extension [.cpp, .hpp, .json, .yaml, .md .docx].')

    arg_parser.add_argument('-g', '--groups',
                            action='store_true',
                            help='Include groups in the output.')
    
    arg_parser.add_argument('-f', '--filter', type=str, nargs='+', 
                            help='Filter Params by groups.')

    arg_parser.add_argument('-v', '--verbose', action='store_true',
                            help='Display extra info.')




    # Parse the arguments
    args = arg_parser.parse_args()

    #try:
    s3trans = S3Transpiler(instance_file=args.file)
    #except Exception as e:
    #    print(e)
    #    exit(-1)


    if(args.out):
        s3trans.render(args.out, args.groups, args.filter)
        
        if args.verbose:
            s3trans.print_stats()
            
        print(f"\nOutput written to {args.out}")





if __name__ == "__main__":
    s3tp_cli()
