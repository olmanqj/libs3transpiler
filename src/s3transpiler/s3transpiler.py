#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
s3 Space System Model Transpiler

Tool to generate code from a s3 Model.

'''

from typing import Any, IO
import os
from pathlib import Path
from jinja2 import Template
import jsonschema
import yaml
import json
import csv
import copy
import pypandoc
from pprint import pprint



class S3Loader(yaml.SafeLoader):
    """YAML Loader with `!include` constructor."""

    def __init__(self, stream: IO) -> None:
        """Initialise Loader."""

        try:
            self._root = os.path.split(stream.name)[0]
        except AttributeError:
            self._root = os.path.curdir

        super().__init__(stream)



def csv_loader(f) -> [dict]:
    # Custom CSV loader to dictionary
    dict_reader = csv.DictReader(f, delimiter=";")
    # Keys to lower case
    dict_reader.fieldnames = [ k.lower() for k in dict_reader.fieldnames]
    list_of_dict = list(dict_reader)
    
    # Construct the System tree
    result = {"subsys":[]}
    for param in list_of_dict:
        # Add the Sys, if dont yet exists
        path = param["sys"].replace('.',':').split(':')
        cur_sys = result
        for sys in path:
            next_sys = None
            if("subsys" in cur_sys.keys()):
                for subsys in cur_sys["subsys"]:
                    if subsys["sys"] == sys:
                        next_sys = subsys
            if next_sys:
                cur_sys = next_sys
            else:
                next_sys = {"sys": sys, "params": []}
                if("subsys" not in cur_sys.keys()):
                    cur_sys["subsys"] = []
                cur_sys["subsys"].append(next_sys)
                cur_sys = next_sys

        ## Add the param
        type_name = param["type"].split(':')

        param_body = {"param": param["param"],
                        "type": type_name[0]}
        
        if len(type_name) > 1:
            param_body["len"] = int(type_name[1])
        if "unit" in param.keys() and  param["unit"]:
            param_body["unit"] = param["unit"]
        if "default" in param.keys() and  param["default"]:
            if(param_body["type"][0] in ["u", "i"]):
                param_body["default"] = int(float(param["max"]))
            elif(param_body["type"][0] in ["f"]):
                param_body["default"] = float(param["default"])
            elif(param_body["type"] == "bool"):
                param_body["default"] = bool(param["default"])
            else:
                param_body["default"] = param["default"]
        if "access" in param.keys() and  param["access"]:
            param_body["access"] = param["access"]
        if "enum" in param.keys() and  param["enum"]:
           param_body["enum"] = param["enum"].replace(" ", "").split(',')
        if "min" in param.keys() and  param["min"]:
            if(param_body["type"][0] in ["u", "i"]):
                param_body["min"] = int(float(param["min"]))
            elif(param_body["type"][0] in ["f"]):
                param_body["min"] = float(param["min"])
        if "max" in param.keys() and  param["max"]:
            if(param_body["type"][0] in ["u", "i"]):
                param_body["max"] = int(float(param["max"]))
            elif(param_body["type"][0] in ["f"]):
                param_body["max"] = float(param["max"])
        if "desc" in param.keys() and  param["desc"]:
            param_body["desc"] = param["desc"]
        if "description" in param.keys() and  param["description"]:
            param_body["desc"] = param["description"]
        if "groups" in param.keys() and  param["groups"]:
            param_body["groups"] = param["groups"].replace(" ", "").split(',')

        cur_sys["params"].append(param_body)
   
    return result["subsys"][0]


def construct_include(loader: S3Loader, node: yaml.Node) -> Any:
    """
    Include file referenced at node.
    https://gist.github.com/joshbode/569627ced3076931b02f
    """

    filename = os.path.abspath(os.path.join(loader._root, loader.construct_scalar(node)))
    extension = os.path.splitext(filename)[1].lstrip('.')

    with open(filename, 'r', encoding='utf-8-sig') as f:
        if extension in ('yaml', 'yml'):
            return yaml.load(f, S3Loader)
        elif extension in ('json', ):
            return json.load(f)
        elif extension in ('csv', ):
            return csv_loader(f)
        else:
            return ''.join(f.readlines())


yaml.add_constructor('!include', construct_include, S3Loader)



class S3Transpiler:


    instance_file = ""    

    schema = dict()
    instance = dict()
    flat_instance = []  # Validated and proceed instance
    groups = dict()
    template = None
    

    # Stats
    total_sys = 0
    total_params = 0
    total_groups = 0


    def __init__(self, instance_file: str) -> None:
        
        # 1. Load the input model
        extension = os.path.splitext(instance_file)[1]
        with open(instance_file, encoding='utf-8-sig') as f:
            if extension in ('.yaml', '.yml'):
                self.instance =  yaml.load(f, S3Loader)
            elif extension in ('.json', ):
                self.instance =  json.load(f)
            elif extension in ('.csv', ):
                self.instance =  csv_loader(f)
            else:
                raise Exception(f"File extension `{extension}` not supported!" )

        # 2. Load the Json schema.
        base_schema_path = Path(__file__).parent / "schemas/s3defs.json"

        # If instance is an array, we need the array schema
        if isinstance(self.instance, list):
            self.s3_schema_path = Path(__file__).parent / "schemas/s3array.json"
        else:
            self.s3_schema_path = Path(__file__).parent / "schemas/s3sys.json"
            
        # Load the s3 schema definitions
        with base_schema_path.open() as f:
            self.defs_schema = json.load(f)

        # Load de complete schema
        with self.s3_schema_path.open() as f:
            self.s3_schema = json.load(f)
            self.s3_schema['$defs'] = self.defs_schema

        # 3. Validate the instance against the schema
        jsonschema.validate(self.instance, self.s3_schema ) 
        print(f"Instance '{instance_file}' is valid!")



    def _filter_params(self, flatten_instance: dict, filters: list):
        # Filter all params and remove those that dont have the specified groups
        result = []    
        for sys in flatten_instance:
            new_params = []
            for param in sys["params"]:
                if "groups" in param:
                    for group in param["groups"]:
                        if group in filters:
                            new_params.append(param)
                            break
            if new_params:
                sys["params"] = new_params
                result.append(sys)
        return result



    def _make_parents(self, sys: dict, parent: str=""):
        # Add parent, path and var_name to each param
        if "params" in sys.keys():
            for param in sys["params"]:
                param["parent"] = f"{parent}{sys['sys']}"
                param["var_name"] = f"{parent}{sys['sys']}_{param['param']}"
                param["path"] = param["parent"].replace("_", ":")
        if "subsys" in sys.keys():
            for subsys in sys["subsys"]:
                subsys["parent"] = f"{parent}{sys['sys']}"
                subsys["var_name"] = f"{parent}{sys['sys']}_{sys['sys']}"
                self._make_parents(subsys, subsys["parent"]+"_")


    def _make_defaults(self, sys: dict):
        # Complete params with the default values for each field
        if "params" in sys:
            for param in sys["params"]:
                if "len" not in param.keys() or not param["len"]:
                    param["len"] = 1
                # Add default if not yet in Param
                if(not "default" in param):
                    if(param["type"] in ["str", "blob"]):
                        param["default"] = ""
                    else:
                        if(param["len"] == 1):
                            param["default"] = 0
                        else:
                            param["default"] = []
                if(not "access" in param):
                    param["access"] = "RO"
                param["size"] = self._calc_param_size(param)
        if "subsys" in sys:
            for subsys in sys["subsys"]:
                self._make_defaults(subsys)


    def _flatten_sys_dict(self, sys: dict) -> list:
        # convert the instance into a list ot systems
        # only accept sys with params, ignore empty sys
        result = []
        #if "params" in sys: # ignore empty sys
        result.append(sys)
        if "subsys" in sys:
            for subsys in sys["subsys"]:
                result += self._flatten_sys_dict(subsys)
            del sys["subsys"]
        return result
    
        
                     
    def _calc_param_size(self, param) -> int:
        if param["type"] in ["str", "blob"]:
            return param["len"]
        elif param["type"] in ["u8", "i8", "bool"]:
            return param["len"]
        elif param["type"] in ["u16", "i16",]:
            return 2 * param["len"]
        elif param["type"] in ["u32", "i32", "f32"]:
            return 4 *param["len"]
        elif param["type"] in ["u64", "i64", "f64"]:
            return 8 * param["len"]
        elif param["type"] == "date":
            return 8 * param["len"]
        elif param["type"] == "time":
            return 4
    

    def _extract_groups(self, flatten_root):

        self.total_groups = 0
        self.total_params = 0
        self.total_sys = 0

        for sys in flatten_root:
            self.total_sys += 1
            if "params" in sys:
                for param in sys["params"]:
                    self.total_params += 1
                    if "groups" in param:
                        for group in param["groups"]:
                            if(group in self.groups.keys()):
                                self.groups[group].append(param)
                            else: 
                                self.groups[group] = [param]
                                self.total_groups += 1


    def get_groups(self, filters:list=None) -> [dict]:
        result = []
        for key, val in self.groups.items():
            if filters and (key not in filters): # Ignore groups not in filters
                continue
            group = []
            offset = 0
            for param in val:
                param_size = self._calc_param_size(param)
                param_body = copy.deepcopy(param)
                #param_body["var_name"] =  param['parent'] + "_" + param['param']
                param_body["offset"] = offset
                # group.append({
                #     "var_name": param['parent'] + "_" + param['param'],
                #     "path": param['path'],
                #     "param": param['param'],
                #     "type": param['type'],
                #     "default": param['default'],
                #     "size": param_size,
                #     "offset": offset,
                #     "unit": param["unit"] if "unit" in param.keys() else "",
                #     "validation": param["validation"] if "validation" in param.keys() else "",
                #     "desc": param["desc"] if "desc" in param.keys() else ""
                # })
                group.append(param_body)
                offset += param_size
            if group:
                result.append({"name": key, "total_size": offset, "params": group})
        return result


    def print_stats(self):
        print(f"\nTotal Systems: {self.total_sys}")
        print(f"Total Params: {self.total_params}")

        groups =  self.get_groups() 
        print(f"Total Groups: {len(groups)}")

        for group in groups:
            print(f"\nGroup {group['name']}")
            print("{: <30} {: <15} {: <15}". format("Param", "Size", "Offset"))
            print("-"*60)
            offset = 0
            for param in group['params']:
                param_path = param['path'] + ":" + param['param']
                print("{: <30} {: <15} {: <15}". format(param_path, param['size'], param['offset']))
            print(f"Total serialized size: {group['total_size']}")



    def _make_flat_instance(self, filters:list):
        # Convert instance into list of systems with params
        
        # Support for single sys or array of sys
        temp_instance = []
        if isinstance(self.instance, list):
            temp_instance  = copy.deepcopy(self.instance)
        else:
            temp_instance  = [copy.deepcopy(self.instance)]

        # Flatten each top-level sys and combine
        for sys in temp_instance:
            self._make_parents(sys , "")
            self._make_defaults(sys )
            self.flat_instance  += self._flatten_sys_dict(sys )

        # Apply group filters and make groups
        if  filters:
            self.flat_instance  = self._filter_params(self.flat_instance, filters)
        self._extract_groups(self.flat_instance)


    def render(self, out_file:str, render_groups:bool=False, filters:list=[]) -> None:

        content = ""
        struct_name, out_file_extension = os.path.splitext(out_file)


        if(out_file_extension == ".yaml" or out_file_extension == ".yml"):
            content = yaml.dump(self.instance, sort_keys=False)

        elif(out_file_extension == ".json"):
            content = json.dumps(self.instance, indent=2, sort_keys=False)
        else:
            template_path = None
            if(out_file_extension == ".cpp"):
                template_path = Path(__file__).parent / "templates/s3.cpp"
            elif(out_file_extension == ".hpp"):
                template_path = Path(__file__).parent / "templates/s3.hpp"
            elif(out_file_extension in [".md", ".docx"]):
                template_path = Path(__file__).parent / "templates/s3.md"
            else:
                raise Exception(f'Output file extension {out_file_extension} not supported!')
            
            # Process the content of instance
            self._make_flat_instance(filters)

            # Create groups list if required
            groups_list = None
            if render_groups:
                groups_list = self.get_groups(filters)

            # Load the template
            with open(template_path) as f:
                self.template = f.read()

            # Render the template
            template = Template(self.template)
            content = template.render(struct_name=struct_name,
                                        #date=datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y"),
                                        sys_list=self.flat_instance,
                                        groups_list=groups_list)



        # Finally, write to file
        out_dir = os.path.dirname(out_file)
        if(out_dir):
            os.makedirs(out_dir, exist_ok=True) # Create folder if it don't exists

        # If output to docx, write with pandoc
        if(out_file_extension == ".docx"):
            with open(out_file, mode="w", encoding="utf-8") as outfile:
                outfile.write(content)
            pypandoc.convert_file(out_file, 'docx', format='md', outputfile=out_file )

        else:
            with open(out_file, mode="w", encoding="utf-8") as outfile:
                outfile.write(content)




