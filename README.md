# s3transpiler


`s3transpiler` is a source-to-source compiler (transpiler) that automatically 
validates and converts [s3 model](#s3-model) into source code in a specified 
target programming language or into documentation.

Supported [s3 model](#s3-model) formats (input file):
- Yaml (Recommended, supports file importing)
- Json 
- CSV

Supported target outputs:
- C++ source (.cpp)
- C++ header (.hpp)
- Json 
- Yaml
- Markdown (documentation)


~~~mermaid
graph LR
    A[s3 Model] ----> B{s3transpiler}
    B ---> C[Source Code]
    B ---> D[s3 Model other formats]
    B ---> E[Documentation]
~~~


## Install CLI tool from source

1. Install build dependencies.
~~~~
pip install  pipx
~~~~

2. Install tool locally.
~~~
pipx install .
~~~

## Install package from source

1. Install package
~~~~
pip install .
~~~~




## Usage

Use CLI tool `s3tp`:

~~~
$ python s3tp file [OPTIONS ...]
~~~

~~~
usage: s3tp [-h] [-o OUT] [-g] [-f FILTER [FILTER ...]] [-v] file

Tool to generate code a and documentation from s3 Model.

positional arguments:
  file                  Path of input s3 model [.yaml, .json, .csv].

optional arguments:
  -h, --help            show this help message and exit
  -o OUT, --out OUT     Name of the output file. Target format will be deduced
                        from provided file extension [.cpp, .hpp, .json,
                        .yaml, .md].
  -g, --groups          Include groups source code. Only valid for source code
                        outputs [.cpp, .hpp].
  -f FILTER [FILTER ...], --filter FILTER [FILTER ...]
                        Filter Params by groups.
  -v, --verbose         Display extra info.
~~~



## s3 Model
See [schemas/s3sys.json](schemas/s3sys.json) for the formal definition.

There are two type of objects in the s3 model:
- `param`: A Parameter. It has the following fields:
  - `param` (required!): The name of the Parameter. Up to 4 alphanumerical chars.
  - `type` (required!): Data type of param. Allowed the following data types:
    - `bool`
    - `u8`
    - `i8`
    - `u16`
    - `i16`
    - `u32`
    - `i32`
    - `u64`
    - `i64`
    - `f32` (IEEE754)
    - `f64` (IEEE754)
    - `date`
    - `str` (utf8)
    - `blob` (base64)

  - `len`: Length of `str`, `blob` or vector Param.
  - `default`: A default value to initialize the Parameter.
  - `access`: Access rights to Parameter from the outside, e.g.: via tele-commands of serialization.
    - `RO`: Read-only (default).
    - `RW`: Read-and-Write.
  - `groups`: An array of tags, to create group of Parameters.
  - `enum`: Array of values. Allow only specific values.
  - `min` & `max`: Allowed range.
  - `unit` : A human friendly unit of measurement.
  - `desc` : A human friendly description.

- `sys`: A System is a container for Parameters and child Systems (called Subsystems). It has the following fields:
  - `sys`: (required!): The name of the System. Up to 4 alphanumerical chars.
  - `params`: Array of `param`.
  - `subsys`: Array of `sys`


A `s3` model document consists of a single `sys` or an array of `sys`.

See [example.yaml](example.yaml) for an example in Yaml format.

### CSV notation

CSV using separator char `;`.

In contrast to Yaml or Json, its not possible to represent a hierarchical structure in CSV. 
Therefore a special notation is required. In a s3 document in CSV format each 
row is a `param`, and requires the following columns. 

- `sys`: the path of the Parameter's parent `sys`, this is the concatenation of
  the names of all predecessor `sys`, separated by `:`. For example `sys:sys:sys`.
- `param`: name of the parameter. Max 4 alphanumerical chars.
- `type`: Data type of param. Allowed the following data types:
    - `bool`
    - `u8`
    - `i8`
    - `u16`
    - `i16`
    - `u32`
    - `i32`
    - `u64`
    - `i64`
    - `f32`
    - `f64`
    - `date`
    - `str:<size>`
    - `blob:<size>`

  
Optional Columns (same as above):
- `default`
- `unit`
- `access`
- `enum`: comma `,` separated array of values.
- `min`
- `max` 
- `groups`
- `description`


